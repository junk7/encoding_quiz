# A test docker file

# Set the base image
FROM ubuntu:20.04

ARG QT_VERSION=5.12.9

# For SDK installation we need /opt to be writable
RUN mkdir -p /opt
RUN chmod 777 /opt

# Prequisites for BSP SDKs
RUN apt-get update && DEBIAN_FRONTEND="noninteractive" TZ="Europe/Berlin" apt-get install -y \
    build-essential git unzip bc cmake curl \
    libc6-i386 lib32stdc++6 lib32z1 qt5-default qttools5-dev-tools libmpfr6 rsync

# install git lfs
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
RUN apt-get install -y git-lfs
RUN git lfs install

ENV HOME /home/jenkins
RUN groupadd -g 10000 jenkins
RUN useradd -c "Jenkins user" -d $HOME -u 10000 -g 10000 -m jenkins
RUN git config --global user.name "Jenkins" && git config --global user.email "jenkins@u-experten.de"


RUN apt update && apt install -y --no-install-recommends apt-transport-https ca-certificates gnupg software-properties-common  \
    wget \
    git \
    openssh-client \
    ca-certificates \
    locales \
    sudo \
    curl \
    build-essential \
    pkg-config \
    libgl1-mesa-dev \
    llvm \
    libclang-dev \
    libssl-dev \
    libudev-dev \
    python \
    cmake \
    lcov \
    gcovr \
    graphviz \
    ninja-build \
    cppcheck \
    parallel \
    libarchive-dev \
    libusb-1.0-0-dev \
    libasound2-dev \
    libgstreamer1.0-dev \
    libgstreamer-plugins-base1.0-dev \
    jq \
    && apt-get -qq clean

# install quicktype
RUN apt-get install -y npm && npm install -g quicktype@15

COPY build-qt.sh /tmp/qt/

# Build qt from source
RUN /tmp/qt/build-qt.sh $QT_VERSION

# install doxygen & Co. for doc generation
RUN apt-get install -y doxygen python3-pip doxyqml #python-pip

# workaround needed as long as SDK is compiled on Ubuntu 16.04
RUN ln -s /usr/lib/x86_64-linux-gnu/libmpfr.so.6 /usr/lib/x86_64-linux-gnu/libmpfr.so.4

# Uuuuh - makes it 4.8GB big ?!
# that would be needed for PDF generation :-(
#RUN apt-get install -y texlive-full
