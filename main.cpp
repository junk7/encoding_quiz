#include <range/v3/view.hpp>
#include <range/v3/action.hpp>
#include <iostream>


std::string encode(const std::string &inputString)
{
    // handle a sequence of equal characters
    // example input: aaa     outpt: 3a
    // example input: b       output: b
    auto encodeBlock = [] (auto in) -> std::string  {
        if(in.size() > 1){
            return  std::to_string(in.size())+in[0];
        }
        return std::string{in[0]};
    };

    using namespace ranges;

    // ranges support pipe | programming, similar to Linux shell
    const std::string encodedString = inputString
                  | ranges::views::group_by( [](char a, char b){return a== b;})
                  | ranges::views::transform(encodeBlock)
                  | actions::join  ;

    const bool encodedIsShorter = encodedString.size() < inputString.size();
    return encodedIsShorter ? encodedString : inputString;
}


int main()
{
    const std::string inputString = "abcccccdefxxx";
    std::cout << encode(inputString) << std::endl;
}
